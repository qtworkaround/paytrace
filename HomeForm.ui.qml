import QtQuick 2.11
import QtQuick.Controls 2.4
import QtCharts 2.2

Page {
    width: 600
    height: 400

    title: qsTr("Home")

    Label {
        text: qsTr("You are on the home page.")
        anchors.centerIn: parent
    }

    ChartView {
        id: bar
        x: 13
        y: 212
        width: 576
        height: 177
        BarSeries {
            name: "BarSeries"
            BarSet {
                label: "Set1"
                values: [2, 2, 3]
            }

            BarSet {
                label: "Set2"
                values: [5, 1, 2]
            }

            BarSet {
                label: "Set3"
                values: [3, 5, 8]
            }
        }
    }

    Button {
        id: button
        x: 65
        y: 47
        text: qsTr("Button")
    }
}
